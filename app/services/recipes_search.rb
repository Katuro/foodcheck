class RecipesSearch

  def self.get_recipes(language, ingredients)
    send("search_#{language}", parsed_ingredients(ingredients))
  end

  protected

  def self.search_en(ingredients)
    recipies = []
    recipes_file = File.open("#{Rails.public_path}/recipes/recipes-en.json")
    recipes_json = JSON.load(recipes_file)

    recipes_json.each do |recipe|
      next unless contain_ingredients?(recipe, ingredients)

      recipies << {
        name: recipe['title'],
        ingredients: recipe['ingredients'].join(', ')
      }
    end

    recipes_file.close
    recipies
  end

  def self.search_fr(ingredients)
    recipies = []
    recipes_file = File.open("#{Rails.public_path}/recipes/recipes-fr.json")
    recipes_json = JSON.load(recipes_file)

    recipes_json.each do |recipe|
      next unless contain_ingredients?(recipe, ingredients)

      recipies << {
        name: recipe['name'],
        ingredients: recipe['ingredients'].join(', ')
      }
    end

    recipes_file.close
    recipies
  end

  def self.contain_ingredients?(recipe, ingredients)
    recipe['ingredients'].join(' ').downcase.match?(ingredients)
  end

  def self.parsed_ingredients(ingredients)
    ingredients.downcase.split('-').collect(&:squish).map{|i| "(?=.*#{i})"}.join
  end

end
