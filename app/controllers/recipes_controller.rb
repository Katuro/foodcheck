class RecipesController < ApplicationController
  require 'resolv-replace'
  protect_from_forgery with: :null_session

  def search
    return if params[:language].nil? || params[:ingredients].nil?
    @recipes = ::RecipesSearch.get_recipes(params[:language], params[:ingredients]).first(25)
  end
end
