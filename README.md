# Setup

ruby 3.0.0
# Description

Food Check get the data from a JSON file scrapped from www.allrecipes.com.
Give it some ingredients and it will return the 25 first recipes it find.


# Usage

You can also simply open this url : https://foodcheck-test.herokuapp.com/
The app is heberged on Heroku.

If you want to start it on your own machine, follow the next steps :

Go in the root directory of the project.

```bash
$> bundle install
$> rails s
```

Then open localhost:3000 in your browser.
