Rails.application.routes.draw do
  root 'home#index'

  post '/recipes/search', to: 'recipes#search'
end
